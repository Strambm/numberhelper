<?php

namespace Cetria\Helpers\NumberHelper\Tests\Numerable;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\NumberHelper\Numerable;
use Cetria\Helpers\NumberHelper\NumberHelper;

class CompatibilityWithHelperTest extends TestCase
{
    /**
     * @test
     */
    public function helperCompatibility(): void
    {
        $methods = $this->arrangeHelperHidenMethods();
        $oposite = $this->arrangeNumerableHidenMethods();
        $this->assertCount(0, array_diff($oposite, $methods));
    }

    /**
     * @test
     */
    public function numerableCompatibility(): void
    {
        $oposite = $this->arrangeHelperHidenMethods();
        $methods = $this->arrangeNumerableHidenMethods();
        $this->assertCount(0, array_diff($oposite, $methods));
    }

    protected function arrangeHelperMethods(): array
    {
        $all = get_class_methods(NumberHelper::class);
        $withoutHidden = array_diff($all, $this->arrangeHelperHidenMethods());
        return $withoutHidden;
    }

    protected function arrangeHelperHidenMethods(): array
    {
        return [
            '__toString',
        ];
    }

    protected function arrangeNumerableMethods(): array
    {
        $all = get_class_methods(Numerable::class);
        $withoutHidden = array_diff($all, $this->arrangeNumerableHidenMethods());
        return $withoutHidden;
    }

    protected function arrangeNumerableHidenMethods(): array
    {
        return [
            '__toString',
        ];
    }
}
