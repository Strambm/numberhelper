<?php

namespace Cetria\Helpers\NumberHelper\Tests\NumberHelper;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\NumberHelper\Numerable;
use Cetria\Helpers\NumberHelper\NumberHelper;

class FloorTest extends TestCase
{
    /**
     * @test
     */
    public function moreThanZero(): void
    {
        $this->assertEquals(51.5148, $this->act('51.514851', 4));
    }

    /**
     * @test
     */
    public function lowerThanZero(): void
    {
        $this->assertEquals(50, $this->act('51.514851', -1));
    }

    /**
     * @test
     */
    public function numerable(): void
    {
        $this->assertEquals(50, $this->act(new Numerable('51.5'), -1));
    }

    protected function act(...$args): float
    {
        $response = NumberHelper::floor(...$args);
        return $response;
    }
}
