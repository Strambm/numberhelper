<?php

namespace Cetria\Helpers\NumberHelper\Tests\NumberHelper;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\NumberHelper\Numerable;
use Cetria\Helpers\NumberHelper\NumberHelper;

class HasSamePolarityTest extends TestCase
{
    /**
     * @test
     */
    public function onlyZero(): void
    {
        $this->assertTrue($this->act(0));
    }

    /**
     * @test
     */
    public function allPossitive(): void
    {
        $this->assertTrue($this->act(1, 2.5, new Numerable(6), '5.5', 0));
    }

    /**
     * @test
     */
    public function allNegative(): void
    {
        $this->assertTrue($this->act(-1, -2.5, new Numerable(-6), '-5.5', 0));
    }

    /**
     * @test
     */
    public function mixedPolarity(): void
    {
        $this->assertFalse($this->act(1, -1));
    }

    protected function act(...$args): bool
    {
        $response = NumberHelper::hasSamePolarity(...$args);
        return $response;
    }
}
