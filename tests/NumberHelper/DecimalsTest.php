<?php

namespace Cetria\Helpers\NumberHelper\Tests\NumberHelper;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\NumberHelper\Numerable;
use Cetria\Helpers\NumberHelper\NumberHelper;

class DecimalsTest extends TestCase
{
    /**
     * @test
     */
    public function zero(): void
    {
        $this->assertEquals(0, $this->act(0));
    }

    /**
     * @test
     */
    public function numerable(): void
    {
        $this->assertEquals(1, $this->act(new Numerable(5.5)));
    }

    /**
     * @test
     */
    public function string(): void
    {
        $this->assertEquals(4, $this->act('5.1234'));
    }

    /**
     * @test
     */
    public function lastNumbersZero(): void
    {
        $this->assertEquals(4, $this->act('1.0000'));
    }

    /**
     * @test
     */
    public function negativeValue(): void
    {
        $this->assertEquals(2, $this->act(-0.15));
    }

    protected function act(...$args): int
    {
        $response = NumberHelper::decimals(...$args);
        return $response;
    }
}
