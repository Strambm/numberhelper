<?php

namespace Cetria\Helpers\NumberHelper\Tests\NumberHelper;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\NumberHelper\Numerable;
use Cetria\Helpers\NumberHelper\NumberHelper;

class IsFloatTest extends TestCase
{
    /**
     * @test
     */
    public function integer(): void
    {
        $this->assertTrue($this->act(0));
    }

    /**
     * @test
     */
    public function numerable(): void
    {
        $this->assertTrue($this->act(new Numerable(7.6)));
    }

    /**
     * @test
     */
    public function string(): void
    {
        $this->assertTrue($this->act('5.84'));
    }

    /**
     * @test
     */
    public function zeroDecimalStringFloat(): void
    {
        $this->assertTrue($this->act('0.00'));
    }

    /**
     * @test
     */
    public function float(): void
    {
        $this->assertTrue($this->act(38.15));
    }

    /**
     * @test
     */
    public function bad(): void
    {
        $this->assertFalse($this->act('15,15'));
    }

    protected function act(...$args): bool
    {
        $response = NumberHelper::isFloat(...$args);
        return $response;
    }
}
