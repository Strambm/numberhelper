<?php

namespace Cetria\Helpers\NumberHelper;

use Stringable;

class Numerable
implements Stringable
{
    /** @var float $value */
    protected $value = 0;

    public function __construct(float $value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function decimals(): int
    {
        return NumberHelper::decimals($this->value);
    }

    public function floor(int $decimals = 0): Numerable
    {
        $this->value = NumberHelper::floor($this->value, $decimals);
        return $this;
    }

    /**
     * @param Numerable|float|int
     */
    public function hasSamePolarity(...$vals): bool
    {
        $vals[] = $this->value;
        return NumberHelper::hasSamePolarity(...$vals);
    }
}
