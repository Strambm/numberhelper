<?php

namespace Cetria\Helpers\NumberHelper;

class NumberHelper
{
    /**
     * @param string|int|float|Numerable $value
     * @see \Cetria\Helpers\NumberHelper\Tests\NumberHelper\FloorTest
     */
    public static function floor($value, int $decimals = 0): float
    {
        $value = static::getValue($value);
        $multiplier = pow(10, $decimals);
        $newValue = floor($value * $multiplier) / $multiplier;
        return $newValue;
    }

    /**
     * @see \Cetria\Helpers\NumberHelper\Tests\NumberHelper\IsFloatTest
     */
    public static function isFloat(string $value): bool
    {
        $value = static::getValue($value);
        return is_numeric($value);
    }

    /**
     * @param string|int|float|Numerable $value
     * @see \Cetria\Helpers\NumberHelper\Tests\NumberHelper\DecimalsTest
     */
    public static function decimals($value): int
    {
        return (int) strpos(strrev($value), ".");
    }

    /**
     * @param Numerable|int|float
     * @see \Cetria\Helpers\NumberHelper\Tests\NumberHelper\HasSamePolarityTest
     */
    public static function hasSamePolarity(...$numbers): bool
    {
        $numbers = array_filter(
            $numbers,
            function($number): bool {
                return $number != 0;
            }
        );
        $numbers = array_map(
            function($number) {
                if($number instanceof Numerable) {
                    return $number->getValue();
                } else {
                    return $number;
                }
            },
            $numbers
        );
        $polarity = current($numbers) > 0;
        foreach($numbers as $number)
        {
            $actualPolarity = $number > 0;
            if($actualPolarity != $polarity) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string|int|float|Numerable $val
     * @return string|int|float
     */
    protected static function getValue($val)
    {
        if($val instanceof Numerable) {
            return $val->getValue();
        }
        return $val;
    }
}
